<?php
declare(strict_types=1);

namespace MVQN\Helpers\Exceptions;

/**
 * Class ArrayHelperException
 *
 * @package MVQN\Helpers\Exceptions
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */
class ArrayHelperException extends \Exception
{
}
