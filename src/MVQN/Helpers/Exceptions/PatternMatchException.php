<?php
declare(strict_types=1);

namespace MVQN\Helpers\Exceptions;

/**
 * Class PatternMatchException
 *
 * @package MVQN\Helpers\Exceptions
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */
class PatternMatchException extends \Exception
{
}