<?php
declare(strict_types=1);

namespace MVQN\Annotations\Exceptions;

/**
 * Class AnnotationReaderException
 *
 * @package MVQN\Annotations
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */
class AnnotationReaderException extends \Exception
{
}