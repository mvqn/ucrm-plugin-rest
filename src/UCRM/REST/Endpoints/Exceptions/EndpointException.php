<?php
declare(strict_types=1);

namespace UCRM\REST\Endpoints\Exceptions;

/**
 * Class EndpointException
 * @package UCRM\REST\Endpoints
 */
class EndpointException extends \Exception
{
}