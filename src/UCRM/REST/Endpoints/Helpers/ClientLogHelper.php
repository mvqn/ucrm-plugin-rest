<?php
declare(strict_types=1);

namespace UCRM\REST\Endpoints\Helpers;

use UCRM\REST\Endpoints\ClientLog;

/**
 * Trait ClientLogHelper
 * @package UCRM\REST\Endpoints\Helpers
 */
trait ClientLogHelper
{
    use Common\ClientHelpers;
    use Common\UserHelpers;

    // =================================================================================================================
    // HELPER METHODS
    // -----------------------------------------------------------------------------------------------------------------



}